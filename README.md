# Github Top Stars #

The idea of this project is to implement a solution for discovering popular repositories on GitHub.
### WService Specification ###
The service should be able to provide:
- A list of the most popular repositories, sorted by number of stars.
- An option to be able to view the top 10, 50, 100 repositories should be available.
- Given a date, the most popular repositories created from this date onwards should be returned.
- A filter for the programming language.
- Note: design this service to be able to handle great load of requests per second

### Implementation Details ###
GitHub provides a public search endpoint which you can use for fetching the most popular repositories: [https://api.github.com/search/repositories?q=created:>2019-01-10&sort=stars&order=desc](https://api.github.com/search/repositories?q=created:>2019-01-10&sort=stars&order=desc) .
- feel free to use any other endpoints, if you wish.
- feel free to use PHP or Node JS.
- feel free to use any libraries or frameworks as you would in a real world solution.

### Solution evaluation ###
Your solution will be evaluate corresponding to these points:

- the solution can be a web API or a CLI application but it's a big bonus if you can enable both in your solution.
- Concise and clean code.
- Scalability and performance.
- Automated tests.
- Using containers (Docker).
- include a readme file for how to use the solution.

### Submit your challenge ###
- Fork the Challenge Repository
- Create a new branch and push your commits to as you would do in a realworld task
- Issue a Pull Request

*NOTE: This challenge is aimed to senior developers, so expected to hit a high point of technicality.*